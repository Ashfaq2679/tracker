package com.tracker.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest	
@AutoConfigureMockMvc
public class TestController {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private RegistrationController controller;

//test "api/v1/register
	@Test
	public void testRegisterWhenUsernameAndPasswordArePresent() throws Exception {
		mockMvc.perform(post("/api/v1/register")).andExpect(status().isOk());
	}
	
	@Test
	public void testRegistetForGet() throws Exception {
		mockMvc.perform(get("/api/v1/register")).andExpect(status().isOk());
	}
}
