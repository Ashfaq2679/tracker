package com.tracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tracker.repository.RegistrationRepository;

@SpringBootApplication
public class TrackerApplication { // implements CommandLineRunner{

	@Autowired
	RegistrationRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(TrackerApplication.class, args);
	}
	/*
	 * @Override public void run(String... args) throws Exception {
	 * 
	 * repository.deleteAll();
	 * 
	 * // save a couple of Repositorys repository.save(new Registration("Alice",
	 * "Smith")); repository.save(new Registration("Bob", "Smith"));
	 * 
	 * // fetch all Repositorys
	 * System.out.println("Repositorys found with findAll():");
	 * System.out.println("-------------------------------"); for (Registration
	 * repository : repository.findAll()) { System.out.println(repository); }
	 * System.out.println();
	 * 
	 * // fetch an individual Repository
	 * System.out.println("Repository found with findByFirstName('Alice'):");
	 * System.out.println("--------------------------------");
	 * System.out.println(repository.findByEmail("Alice")); }
	 */
}
