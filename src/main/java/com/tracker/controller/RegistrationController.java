package com.tracker.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.tracker.model.Registration;
import com.tracker.repository.RegistrationRepository;

@RestController
@RequestMapping("api/v1")
public class RegistrationController {

	@Autowired
	RegistrationRepository repository;
	
	@GetMapping("/register")
	public String getMessage() {
		return "Running";
	}
	
	@PostMapping("/register-map")
	public List<Registration> register(){
		/*
		 * Mongo mongo = new Mongo("localhost", 27017); DB db = mongo.getDB("sample");
		 * DBCollection collection = db.getCollection("sample1");
		 * 
		 * Map<String, String> map = new HashMap<String, String>(); map.put("name",
		 * "Mongo-Application-1"); map.put("age", "18"); collection.insert(new
		 * BasicDBObject(map)); //Working sand saving data to DB
		 */
		
		Registration map = new Registration("test@test.com","pass");
		map.put("name", "Mongo-Application-3");
		map.put("age", "21");
		repository.insert(map);
		return repository.findAll();
	}
	
	@PostMapping("/register")
	public DBCursor registerUsingMap(){
		Mongo mongo = new Mongo("localhost", 27017);
		DB db = mongo.getDB("sample");	
		DBCollection collection = db.getCollection("sample1");
		

		Registration reg = new Registration("ash1988", "pass");
		//collection.insert(reg);
		repository.save(reg);
		return null;
	}
}
