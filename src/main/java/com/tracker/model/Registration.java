package com.tracker.model;

import java.util.HashMap;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Registration extends HashMap{
	@Id
	private String id;
	private String email;
	private String password;
	private boolean active;
	
	public Registration() {
		
	}

	public Registration(String email, String password) {
		this.put("email",email);
		this.put("password",password);
	}
}
