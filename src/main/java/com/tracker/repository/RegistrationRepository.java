package com.tracker.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.mongodb.DBObject;
import com.tracker.model.Registration;

@Repository
public interface RegistrationRepository extends MongoRepository<Registration, String>{
	public List<Registration> findByActive();
	public Registration findByEmail(String email);
	public void save(DBObject obj);
}
